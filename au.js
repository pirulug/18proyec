< script type = "61ac61cfde1c2ded4579d327-text/javascript" >


    $(".card__header").append('<a class="btn color_web" href="https://youtu.be/s3-m7iw62Iw" target="_blank">Guía de configuración</a>');


var acortamiento_api = "all";

if (acortamiento_api != null) {

    $('#acort_logic option').each(function (e) {

        $(this).val();

        if ($(this).val() == acortamiento_api) {

            $(this).prop('selected', true);

        }

    });

}


var service_id_api = "32";

if (service_id_api != null) {

    $('#service_id option').each(function (e) {

        $(this).val();

        if ($(this).val() == service_id_api) {

            $(this).prop('selected', true);

        }

    });

}


var domain_permision = "all";

if (domain_permision != null) {

    $('#domains_permisions option').each(function (e) {

        $(this).val();

        if ($(this).val() == domain_permision) {

            $(this).prop('selected', true);

        }

    });

}



var has_shorturl_enabled = "1";

if (has_shorturl_enabled != null) {

    $('#has_shorturl_enabled option').each(function (e) {

        $(this).val();

        if ($(this).val() == has_shorturl_enabled) {

            $(this).prop('selected', true);

        }

    });

}


var list_bucle = "listado";

if (list_bucle != null) {

    $('#acortamiento_logica_elegida option').each(function (e) {

        $(this).val();

        if ($(this).val() == list_bucle) {

            $(this).prop('selected', true);

        }

    });

}

var domain_permision = "all";



if (domain_permision != null) {

    $('#domain_permision2 option').each(function (e) {

        $(this).val();



        if ($(this).val() == domain_permision) {

            $(this).prop('selected', true);



        }

    });

}


$('#service_id').change(function (event) {
    let service = $("#service_id option:selected").val();

    if (service == "31") {

        $("#User_ID").removeClass('display_no');
        $("#User_ID").addClass('display_si');
        $(".select_acort").addClass('mediumblock');
        $(".select_acort").removeClass('allblock');
    } else {
        $("#User_ID").removeClass('display_si');
        $("#User_ID").addClass('display_no');
        $(".select_acort").removeClass('mediumblock');
        $(".select_acort").addClass('allblock');
    }
});

var service = $("#service_id option:selected").val();

if (service == "31") {

    $("#User_ID").removeClass('display_no');
    $("#User_ID").addClass('display_si');
    $(".select_acort").addClass('mediumblock');
    $(".select_acort").removeClass('allblock');
} else {
    $("#User_ID").removeClass('display_si');
    $("#User_ID").addClass('display_no');
    $(".select_acort").removeClass('mediumblock');
    $(".select_acort").addClass('allblock');
}


$('#acort_logic').change(function (event) {
    let logica = $("#acort_logic option:selected").val();
    if (logica == "all") {

        $(".firs_accort").removeClass('display_si');
        $(".firs_accort").removeClass('flex_only');
        $(".firs_accort").addClass('display_no');
        $(".all").removeClass('display_no');
        $("#bloque_all").addClass('flex');
        $("#listadoobucle").addClass('flex');
        $(".all").addClass('display_si');
    } else {
        $(".firs_accort").removeClass('display_no');
        $(".firs_accort").addClass('flex_only');
        $(".firs_accort").addClass('display_si');
        $(".all").addClass('display_no');
        $("#bloque_all").removeClass('flex');
        $("#listadoobucle").removeClass('flex');
        $(".all").removeClass('display_si');
    }
});


var logica = $("#acort_logic option:selected").val();

var logica = $("#acort_logic option:selected").val();
if (logica == "all") {

    $(".firs_accort").removeClass('display_si');
    $(".firs_accort").removeClass('flex_only');
    $(".firs_accort").addClass('display_no');
    $(".all").removeClass('display_no');
    $("#bloque_all").addClass('flex');
    $("#listadoobucle").addClass('flex');
    $(".all").addClass('display_si');
} else {
    $(".firs_accort").removeClass('display_no');
    $(".firs_accort").addClass('flex_only');
    $(".firs_accort").addClass('display_si');
    $(".all").addClass('display_no');
    $("#bloque_all").removeClass('flex');
    $("#listadoobucle").removeClass('flex');
    $(".all").removeClass('display_si');
}


var urls = [];

$("#lista_urls option").each(function (index, el) {
    let id = $(this).val();
    let title = $(this).text();

    if (id != null && title != null) {

        urls.push([id, title]);

    }
});

var data_apis = [];

var count_data_api = 0;

$(".data_user_api").each(function (index, el) {

    let service_api_data = $("#service_api_data" + count_data_api).val();
    let service_api_key = $("#service_api_key" + count_data_api).val();
    let service_api_user = $("#service_api_user" + count_data_api).val();

    data_apis.push([service_api_data, service_api_key, service_api_user]);

    count_data_api++;

});


var data = "1";


if (data != null) {

    $("#logica_acort option").each(function (index, el) {

        $(this).val();

        if ($(this).val() == data) {

            $(this).prop('selected', true);

        }

    });

    var pastes_acort_number = "all";

    $("#num_acort_topaste option").each(function (index, el) {

        $(this).val();

        if ($(this).val() == pastes_acort_number) {

            $(this).prop('selected', true);

        }

    });


    localStorage.setItem('count_old', data_apis.length);


    var wrapper_list_acorts = $("#list_all_accorts");

    var count = 1;

    for (var i = 0; i < data_apis.length; i++) {

        var contador = i + count;

        $(wrapper_list_acorts).append('<div class="flex all lista_acortadores" id="elemento_data' + contador + '"><div  class="mediumblock"><div class="form__block"><label for="acortamiento_api" class="label_acortador">Acortador ' + contador + '</label><select class="form-control listas_acortadores_api" name="logica_acort_bucle[]" id="service_list' + i + '"></select></div></div><div class="mediumblock flex"><div class="form__block allblock select_acort' + i + '"><label for="api_key">Llave API</label><input class="form-control" type="text" name="api_key_bucle[]" value="' + data_apis[i][1] + '" id="api_key_all' + i + '"></div><div class="form__block display_no" style="flex-basis: calc(50% - 1rem);" id="User_ID' + i + '"><label for="User_ID">User ID</label><input class="form-control" type="text" name="User_ID_bucle[]" value="" id="User_ID_data' + i + '"></div></div></div><input type="hidden" id="service_list' + i + 'control" value="' + i + '">');

        giveList(i);
        marcaLista(i, data_apis[i][0], data_apis[i][2]);


    }



} else {

    var list_all_accorts = $("#logica_acort option:selected").val();

    var wrapper_list_acorts = $("#list_all_accorts");

    var count = 1;

    for (var i = 0; i < list_all_accorts; i++) {

        var contador = i + count;

        $(wrapper_list_acorts).append('<div class="flex all lista_acortadores"><div  class="mediumblock"><div class="form__block"><label for="acortamiento_api">Acortador ' + contador + '</label><select class="form-control" name="logica_acort_bucle[]" id="service_list' + i + '"></select></div></div><div class="mediumblock flex"><div class="form__block allblock select_acort' + i + '"><label for="api_key">Llave API</label><input class="form-control" type="text" name="api_key_bucle[]" value="" id="api_key_all' + i + '"></div><div class="form__block display_no" style="flex-basis: calc(50% - 1rem);" id="User_ID' + i + '"><label for="User_ID">User ID</label><input class="form-control" type="text" name="User_ID_bucle[]" value=""></div></div></div><input type="hidden" id="service_list' + i + 'control" value="' + i + '">');

        giveList(i);

    }


}


if (data == "") {


    var list_all_accorts = $("#logica_acort option:selected").val();

    var wrapper_list_acorts = $("#list_all_accorts");

    var count = 1;

    for (var i = 0; i < list_all_accorts; i++) {

        var contador = i + count;

        $(wrapper_list_acorts).append('<div class="flex all lista_acortadores"><div  class="mediumblock"><div class="form__block"><label for="acortamiento_api">Acortador ' + contador + '</label><select class="form-control" name="logica_acort_bucle[]" id="service_list' + i + '"></select></div></div><div class="mediumblock flex"><div class="form__block allblock select_acort' + i + '"><label for="api_key">Llave API</label><input class="form-control" type="text" name="api_key_bucle[]" value="" id="api_key_all' + i + '"></div><div class="form__block display_no" style="flex-basis: calc(50% - 1rem);" id="User_ID' + i + '"><label for="User_ID">User ID</label><input class="form-control" type="text" name="User_ID_bucle[]" value=""></div></div></div><input type="hidden" id="service_list' + i + 'control" value="' + i + '">');

        giveList(i);

    }

}



if (data != null) {

    localStorage.setItem('count_old', data_apis.length);



    $("#logica_acort").change(function (event) {


        var count = $("#logica_acort option:selected").val();

        let wrapper_list_acorts = $("#list_all_accorts");

        var contador_viejo = localStorage.getItem('count_old');

        if (count > parseInt(contador_viejo)) {

            for (var i = parseInt(contador_viejo); i < count; i++) {

                $(wrapper_list_acorts).append('<div class="flex all lista_acortadores" id="elemento_data' + i + '"><div  class="mediumblock"><div class="form__block"><label for="acortamiento_api" class="label_acortador">Acortador ' + i + '</label><select class="form-control" name="logica_acort_bucle[]" id="service_list' + i + '"></select></div></div><div class="mediumblock flex"><div class="form__block allblock select_acort' + i + '"><label for="api_key">Llave API</label><input class="form-control" type="text" name="api_key_bucle[]" value="" id="api_key_all' + i + '"></div><div class="form__block display_no" style="flex-basis: calc(50% - 1rem);" id="User_ID' + i + '"><label for="User_ID">User ID</label><input class="form-control" type="text" name="User_ID_bucle[]" value=""></div></div></div><input type="hidden" id="service_list' + i + 'control" value="' + i + '">');

                giveList(i);


                $('#service_list' + i).change(function (event) {

                    let id = $(this).attr('id');

                    let valor = giveValue(id);

                    let service = $("option:selected", this).val();

                    if (service == "31") {

                        $('#User_ID' + valor).removeClass('display_no');
                        $('#User_ID' + valor).addClass('display_si');
                        $('.select_acort' + valor).addClass('mediumblock');
                        $('.select_acort' + valor).removeClass('allblock');
                    } else {
                        $('#User_ID' + valor).removeClass('display_si');
                        $('#User_ID' + valor).addClass('display_no');
                        $('.select_acort' + valor).removeClass('mediumblock');
                        $('.select_acort' + valor).addClass('allblock');
                    }
                });

            }



        } else {

            dameContador(count, contador_viejo);

        }

        localStorage.setItem('count_old', count);

        var contador_label = 1;
        $(".label_acortador").each(function (index, el) {
            $(this).text("Acortador " + contador_label);
            contador_label++;
        });



    });


} else {


    var count2 = 1;

    $("#logica_acort").change(function (event) {

        let count = $("#logica_acort option:selected").val();

        let wrapper_list_acorts = $("#list_all_accorts");

        $('.lista_acortadores').each(function (index, el) {
            $(this).remove();
        });

        for (var i = 0; i < count; i++) {

            var contador2 = i + count2;

            $(wrapper_list_acorts).append('<div class="flex all lista_acortadores"><div  class="mediumblock"><div class="form__block"><label for="acortamiento_api">Acortador ' + contador2 + '</label><select class="form-control" name="logica_acort_bucle[]" id="service_list' + i + '"></select></div></div><div class="mediumblock flex"><div class="form__block allblock select_acort' + i + '"><label for="api_key">Llave API</label><input class="form-control" type="text" name="api_key_bucle[]" value="" id="api_key_all' + i + '"></div><div class="form__block display_no" style="flex-basis: calc(50% - 1rem);" id="User_ID' + i + '"><label for="User_ID">User ID</label><input class="form-control" type="text" name="User_ID_bucle[]" value=""></div></div></div><input type="hidden" id="service_list' + i + 'control" value="' + i + '">');

            giveList(i);

            $('#service_list' + i).change(function (event) {

                let id = $(this).attr('id');

                let valor = giveValue(id);

                let service = $("option:selected", this).val();

                if (service == "31") {

                    $('#User_ID' + valor).removeClass('display_no');
                    $('#User_ID' + valor).addClass('display_si');
                    $('.select_acort' + valor).addClass('mediumblock');
                    $('.select_acort' + valor).removeClass('allblock');
                } else {
                    $('#User_ID' + valor).removeClass('display_si');
                    $('#User_ID' + valor).addClass('display_no');
                    $('.select_acort' + valor).removeClass('mediumblock');
                    $('.select_acort' + valor).addClass('allblock');
                }
            });

        }
    });

}



function giveList(i) {

    var contenedor = $("#service_list" + i);

    for (var i = 0; i < urls.length; i++) {
        $(contenedor).append('<option value="' + urls[i][0] + '">' + urls[i][1] + '</option>');
    }


}



function giveValue(data) {

    window[data] = '#' + data + 'control';

    let num = $(window[data]).val();

    return num;

}



function marcaLista(data, data2, data3) {

    $("#service_list" + data + " option").each(function (index, el) {

        $(this).val();

        if ($(this).val() == data2) {

            $(this).prop('selected', true);

        }

    });


    $("#User_ID_data" + data).attr('value', data3);
}


function dameContador(data, data2) {




    for (var i = parseInt(data); i < parseInt(data2); i++) {

        $('#elemento_data' + i).remove();
    }

}



for (var i = 0; i < 50; i++) {

    $('#service_list' + i).change(function (event) {

        let id = $(this).attr('id');

        let valor = giveValue(id);

        let service = $("option:selected", this).val();

        if (service == "31") {

            $('#User_ID' + valor).removeClass('display_no');
            $('#User_ID' + valor).addClass('display_si');
            $('.select_acort' + valor).addClass('mediumblock');
            $('.select_acort' + valor).removeClass('allblock');
        } else {
            $('#User_ID' + valor).removeClass('display_si');
            $('#User_ID' + valor).addClass('display_no');
            $('.select_acort' + valor).removeClass('mediumblock');
            $('.select_acort' + valor).addClass('allblock');
        }
    });

}





$('.listas_acortadores_api').each(function (index, el) {

    let id = $(this).attr('id');

    let valor = giveValue(id);

    let service = $("option:selected", this).val();

    if (service == "31") {

        $('#User_ID' + valor).removeClass('display_no');
        $('#User_ID' + valor).addClass('display_si');
        $('.select_acort' + valor).addClass('mediumblock');
        $('.select_acort' + valor).removeClass('allblock');
    } else {
        $('#User_ID' + valor).removeClass('display_si');
        $('#User_ID' + valor).addClass('display_no');
        $('.select_acort' + valor).removeClass('mediumblock');
        $('.select_acort' + valor).addClass('allblock');
    }

});



$("#Guardar").click(function (event) {
    let valor = $("#acort_logic option:selected").val();

    let pass = true;

    if (valor == "firts") {

        let selected = $("#service_id option:selected").val();

        let user_id = $("#user_id_first").val();

        let user_key = $("#api_key_first").val();

        if (selected != "1") {

            if (selected == "31") {

                if (user_id == "" || user_id == "null" && user_key == "" || user_key == "null") {

                    pass = false;

                }


            } else {

                if (user_key == "" || user_key == "null") {

                    pass = false;

                }

            }


        } else {

            pass = false;

        }

    } else {


        for (var i = 0; i < 50; i++) {

            let service_id_all = $('#service_list' + i).val();


            if (service_id_all) {

                if (service_id_all == "1") {

                    pass = false;

                }

                if (service_id_all == "31") {

                    user_id = $("#User_ID_data" + i).val();

                    user_key = $("#api_key_all" + i).val();


                    if (user_id == "" || user_id == "null" || user_key == "" || user_key == "null") {

                        pass = false;


                    }

                }


                if (service_id_all != "31" && service_id_all != "1" && service_id_all) {


                    user_key = $("#api_key_all" + i).val();

                    if (user_key == "" || user_key == "null") {

                        pass = false;


                    }



                }


            }


        }


    }


    if (pass) {

        $("#manda").click();


    } else {

        Swal.fire({
            icon: 'error',
            title: 'Error al llenar el formulario',
            text: 'Debe elegir un acortador y poner las llave API y en caso de Adf Fly el User ID',
        })

    }


});

<
/script>
