<?php
function showBBcodes($text) {
	// BBcode array
	$find = array(
		'~\[b\](.*?)\[/b\]~s',
		'~\[i\](.*?)\[/i\]~s',
		'~\[u\](.*?)\[/u\]~s',
		'~\[quote\](.*?)\[/quote\]~s',
		'~\[size=(.*?)\](.*?)\[/size\]~s',
		'~\[color=(.*?)\](.*?)\[/color\]~s',
		'~\[url\]((?:ftp|https?)://.*?)\[/url\]~s',
		'~\[ouo\]((?:ftp|https?)://.*?)\[/ouo\]~s',
		'~\[youtube\](https?:\/\/(www.)?youtube.com)?\/(v\/|watch\?v\=|watch\?feature\=player_embedded\&v\=|watch\?feature\=player_detailpage\&v\=)([-|~_0-9A-Za-z]+)&?.*?\[\/youtube\]~s',
		'~\[img\](https?://.*?\.(?:jpg|jpeg|gif|png|bmp))\[/img\]~s'
	);

	// HTML tags to replace BBcode
	$replace = array(
		'<b>$1</b>',
		'<i>$1</i>',
		'<span style="text-decoration:underline;">$1</span>',
		'<pre>$1</'.'pre>',
		'<span style="font-size:$1px;">$2</span>',
		'<span style="color:$1;">$2</span>',
		'<a href="$1">$1</a>',
		'<a href="$1">$1</a>',
		'<iframe width="600" height="400" src="https://www.youtube.com/embed/$4" frameborder="0" allowfullscreen></iframe>',
		'<img src="$1" alt="" />'
	);

	// Replacing the BBcodes with corresponding HTML tags
	return preg_replace($find,$replace,$text);
}

// How to use the above function:

$bbtext = "This is [b]bold[/b] and this is [u]underlined[/u] and this is in [i]italics[/i] with a [color=red] red color[/color][ouo]https://ouo.io/manage/tools/quick-link[/ouo][url]https://ouo.io/manage/tools/quick-link[/url][youtube]https://www.youtube.com/watch?v=p-LOXXGGeAc&src=Linkfire&lId=bb5413b2-b9e2-4ec0-86ff-612e0fdb93e0&cId=d3d58fd7-4c47-11e6-9fd0-066c3e7a8751[/youtube]";
$htmltext = showBBcodes($bbtext);
echo $htmltext;